ARG GO_VER=1.17.8
ARG WITNESS_REF=159-backreference-subjects

FROM golang:$GO_VER
RUN apt update && apt install -y \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    git \
    curl \
    podman 

RUN apt install git docker curl -y

RUN curl -LO https://github.com/snyk/snyk/releases/download/v1.858.0/snyk-linux && \
    chmod +x snyk-linux && \
    mv snyk-linux /usr/local/bin/snyk

RUN curl -LO https://github.com/anchore/syft/releases/download/v0.38.0/syft_0.38.0_linux_amd64.tar.gz && \
    tar -xzf syft_0.38.0_linux_amd64.tar.gz && \
    mv syft /usr/local/bin/syft

COPY ./witness /usr/local/bin/witness

RUN mkdir -p /code

WORKDIR /code
