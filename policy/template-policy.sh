#/bin/sh

if which yq > /dev/null; then
    yq --version
else
    echo "installing yq - need sudo"
    VERSION=v4.2.0 BINARY=yq_linux_amd64

    sudo wget https://github.com/mikefarah/yq/releases/download/${VERSION}/${BINARY} -O /usr/bin/yq &&\
    sudo chmod +x /usr/bin/yq
fi

spiffeID="spiffe://dev.testifysec.com/witness-demo/builder"

#generate policy keys (support for hardware tokens planned)
openssl genpkey -algorithm ed25519 -outform PEM -out testkey.pem
openssl pkey -in testkey.pem -pubout > testpub.pem

#get trust bundle
kubectl -n=spire get cm spire-bundle -o=json | jq -r '.data["bundle.crt"]' > bundle.crt.tmp
bundle=$(cat bundle.crt.tmp)

#inject rego

# #get b64 of policy
# gcp_b64="$(openssl base64 -A <"gcp.rego")"
# git_b64="$(openssl base64 -A <"git.rego")"
# gitlab_b64="$(openssl base64 -A <"gitlab.rego")"
# cmd_b64="$(openssl base64 -A <"cmd.rego")"

bundleb64=$(echo -n "${bundle}" | base64)

cp policy-template.yaml policy.tmp.yaml



rootid=`sha256sum bundle.crt.tmp | awk '{print $1}'`
sed -i "s/{{ROOT_ID}}/$rootid/g" policy.tmp.yaml
sed -i "s#{{SPIFFEID}}#$spiffeID#g" policy.tmp.yaml



yq eval ".roots.${rootid}.certificate = \"${bundleb64}\"" --inplace policy.tmp.yaml


#sed -i "s/{{B64_POLICY_MODULE}}/$rego_b64/g" policy.tmp.json

#norm
yq e -j policy.tmp.yaml > policy.tmp.json

#sign policy with witness
witness sign -k testkey.pem -f policy.tmp.json -o policy.signed.json

